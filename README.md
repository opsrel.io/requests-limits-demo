# Requests & Limits demo

## Cluster bootstrapping

```
$ civo
```

### ArgoCD Install

```
$ helm repo add argo-cd https://argoproj.github.io/argo-helm
$ helm repo update
$ helm install argo-cd argo-cd/argo-cd --namespace argocd --create-namespace

$ kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

$ kubectl port-forward service/argo-cd-argocd-server -n argocd 8080:443
```

### Prometheus deployment


