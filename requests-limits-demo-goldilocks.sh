#!/bin/bash
########################
# include the magic
# https://github.com/paxtonhare/demo-magic
########################
. demo-magic.sh
clear

export KUBECONFIG=${PWD}/mks-1-kubeconfig

# Set demo-magic options
TYPE_SPEED=40 # Accelerate typing
DEMO_CMD_COLOR="" # No bold
DEMO_PROMPT="${PURPLE}$ ${COLOR_RESET}"
DEMO_COMMENT_COLOR=$CYAN

# Save original PWD
_PWD=${PWD}

pe "# Go to Goldilocks website"
open https://goldilocks.docs.fairwinds.com/ 2>&1 > /dev/null

p "# Install VPA and Goldilocks as Helm Chart"
pi "helm repo add fairwinds-stable https://charts.fairwinds.com/stable"
pi "helm install vpa fairwinds-stable/vpa --namespace vpa --create-namespace \\
  --set admissionController.enabled=\"false\" \\
  --set updater.enabled=\"false\" \\
  --set recommender.extraArgs.prometheus-address=\"http://prometheus-operated.observability.svc.cluster.local:9090\" \\
  --set recommender.extraArgs.storage: \"prometheus\""
p "helm install goldilocks fairwinds-stable/goldilocks --namespace vpa \\
  --set dashboard.ingress.enabled=\"true\" \\
  --set dashboard.ingress.annotations=\"...\" \\
  --set dashboard.ingress.hosts=\"...\""

pi "# Check everything is running"
# Set kubecolor alias
pe "kubecolor get pods -n vpa"

pi "# Check VPAs"
pe "kubecolor get vpa -A"

# Add labels, and check 
pi "# Add labels, and check VPA, automatically created by Goldilocks (see K9S)"
pe "kubectl label ns default goldilocks.fairwinds.com/enabled=true"
pei "kubectl label ns boinc goldilocks.fairwinds.com/enabled=true"
pei "kubectl label ns vpa goldilocks.fairwinds.com/enabled=true"
#pe "kubecolor get vpa -A"

# Check dashboard
pe "# Go to Goldilocks dashboard"
open https://goldilocks.devoxxfr.opsrel.io  2>&1 > /dev/null

## Redo again
#pi "# Check VPA again"
#pe "kubecolor get vpa -A"

pi "# Check VPA recommendation"
pe "kubectl get vpa -n default goldilocks-red -o yaml | yq '.status.recommendation'"

#pi "# Now check CustomResourceStateMetrics"
#pe "kubectl get configmap -n observability kube-prometheus-stack-kube-state-metrics-customresourcestate-config -o yaml | yq '.data.\"config.yaml\"'"
#pe "# See Grafana dashboard on the next slide!"

# Unset VPA
pi "# Remove VPAs (see K9S)"
#pe "kubectl label ns default goldilocks.fairwinds.com/enabled-"
pei "kubectl label ns boinc goldilocks.fairwinds.com/enabled-"
pei "kubectl label ns vpa goldilocks.fairwinds.com/enabled-"

## Redo again
#pe "kubecolor get vpa -A"

# Return to the default PWD
cd ${_PWD}

pe ""
