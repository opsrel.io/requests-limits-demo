#!/bin/bash
########################
# include the magic
# https://github.com/paxtonhare/demo-magic
########################
. demo-magic.sh
clear

export KUBECONFIG=${PWD}/mks-1-kubeconfig

# Set demo-magic options
TYPE_SPEED=40 # Accelerate typing
DEMO_CMD_COLOR="" # No bold
DEMO_PROMPT="${PURPLE}$ ${COLOR_RESET}"
DEMO_COMMENT_COLOR=$CYAN

# Save original PWD
_PWD=${PWD}

pe "# Go to Krr website"
open https://github.com/robusta-dev/krr 2>&1 > /dev/null

# Set Python Environment
mkdir -p krr-env
p "# Clone, Python venv and install requirements"
pi "python3 -m venv krr-env"
pei "source krr-env/bin/activate"

# Clone & Install Krr
pi "git clone https://github.com/robusta-dev/krr"
pei "cd krr"
pi "pip install -r requirements.txt"

# Use Krr
pe "python krr.py simple -n default"
pe "python krr.py simple -p https://prometheus.devoxxfr.opsrel.io -n default"
pe "python krr.py simple -p https://prometheus.devoxxfr.opsrel.io -n default -s app=red -q -f json | jq '.scans[0].recommended.requests'"

# Java Strategy
p "# Extend KRR, using custom strategy"
pe "cp ../java_strategy.py ."
pe "python ./java_strategy.py java -p https://prometheus.devoxxfr.opsrel.io -n default -s app=red"
rm java_strategy.py

# K9S Plugin
pi "# Check K9S plugin"

# Return to the default PWD
cd ${_PWD}
deactivate

pe ""
